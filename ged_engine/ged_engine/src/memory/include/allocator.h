class Allocator
{
public:

	Allocator();
	virtual ~Allocator();

	virtual void* Allocate(size_t t, size_t alignment, size_t frontBound) = 0; //t = size inklusive front und back bound
	virtual void Free(void* ptr) = 0;
	virtual size_t getAllocationSize(void* ptr) = 0;
};