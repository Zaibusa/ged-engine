class BoundsChecker
{
	public:
	BoundsChecker();
	BoundsChecker(size_t front, size_t back);
	virtual ~BoundsChecker();

	virtual void CreateFrontBound(void* ptr) = 0; //Writes a specific code into the first frontBound bytes
	virtual void CreateBackBound(void* ptr) = 0;
	virtual void CheckFrontBound(void* ptr) = 0; // Checks if information is still consistent
	virtual void CheckBackBound(void* ptr) = 0;
	size_t getFrontBound();
	size_t getBackBound();
	
	protected:
			size_t frontBound, backBound;
	private:
			virtual void printWarning(std::string warning) = 0;
};