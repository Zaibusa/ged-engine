class MasterMemoryTracker : public MemoryTracker
{

public:
    struct trackingInfo{
        void* allocation;
        size_t size;
        size_t alignment;
        SourceInfo sourceInfo;
    };

public:
	void OnAllocation(void* allocation, size_t size, size_t alignment, SourceInfo sourceInfo);
	void OnDeallocation(void* allocation);
    std::vector<trackingInfo> getRegisteredTrackings();

private:
    std::vector<trackingInfo> registeredTrackings;
};