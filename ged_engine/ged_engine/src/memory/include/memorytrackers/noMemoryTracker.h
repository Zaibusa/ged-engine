class NoMemoryTracker : public MemoryTracker
{
public:
	void OnAllocation(void* allocation, size_t size, size_t alignment, SourceInfo sourceInfo);
	void OnDeallocation(void* allocation);
};