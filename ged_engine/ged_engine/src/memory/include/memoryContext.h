class MemoryContext
{
public:
	MemoryContext(Allocator*, BoundsChecker*, MemoryTracker*);
	~MemoryContext();

	void* Allocate(size_t t, size_t alignment, SourceInfo sourceInfo);
	void Free(void* ptr);
private:
	Allocator* allocator;
	BoundsChecker* boundsChecker;
	MemoryTracker* memoryTracker;
};