class OperatorHelper
{
public:
	template <typename MYTYPE, class MYCONTEXT>
	static void Delete(MYTYPE* object, MYCONTEXT& context)
	{
		object->~MYTYPE();
		context->Free(object);
	}
};