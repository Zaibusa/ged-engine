class MemoryTracker
{
public:
	MemoryTracker();
	virtual ~MemoryTracker();

	virtual void OnAllocation(void* allocation, size_t size, size_t alignment, SourceInfo sourceInfo) = 0;
	virtual void OnDeallocation(void* allocation) = 0;
};