class SimpleBoundsChecker : public BoundsChecker
{
public:
	SimpleBoundsChecker();
	SimpleBoundsChecker(size_t, size_t);

	void CreateFrontBound(void* ptr);
	void CreateBackBound(void* ptr);
	void CheckFrontBound(void* ptr);
	void CheckBackBound(void* ptr);
private:
	void printWarning(std::string warnings);
};