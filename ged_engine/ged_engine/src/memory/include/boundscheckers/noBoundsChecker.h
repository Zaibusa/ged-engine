class NoBoundsChecker : public BoundsChecker
{
public:
	~NoBoundsChecker();
	void CreateFrontBound(void* ptr);
	void CreateBackBound(void* ptr);
	void CheckFrontBound(void* ptr);
	void CheckBackBound(void* ptr);

private:
	void printWarning(std::string warning);
};