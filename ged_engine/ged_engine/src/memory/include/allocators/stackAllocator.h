class StackAllocator : public Allocator
{
public:
	StackAllocator(size_t size, size_t alignment, size_t bounds);			// Creates the memory stack and aligns it to "alignment"
	StackAllocator::~StackAllocator();
	void* Allocate(size_t size, size_t alignment, size_t frontBound);		// Allocates the next free aligned block of "size" on the stack
	void Free(void* ptr);								// Frees the space and moves the current pointer on the stack. Meant to only remove topmost object on stack
	void freeStack();									// Frees all space allocated by the stack
	size_t getAllocationSize(void* ptr);
private:
	char* m_start, *m_current, *m_end;					// Pointers to: beginning of stack (unaligned), current position, last position on stack
};