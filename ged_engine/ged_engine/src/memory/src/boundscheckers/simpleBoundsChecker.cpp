#include "../../../../include/moduleMemory.h"

SimpleBoundsChecker::SimpleBoundsChecker()
{

}

SimpleBoundsChecker::SimpleBoundsChecker(size_t front, size_t back){
	this->frontBound = front;
	this->backBound = back;
}

//sets bound before offset and allocation
void SimpleBoundsChecker::CreateFrontBound(void* ptr){
	for(int i = 0; i < frontBound; ++i){
		((char*)ptr)[i] = 0xFB;
	}
}

//sets bound after allocation
void SimpleBoundsChecker::CreateBackBound(void* ptr){
	for(int i = 0; i < backBound; ++i){
		((char*)ptr)[i] = 0xBB;
	}
}

void SimpleBoundsChecker::CheckFrontBound(void* ptr){
	for(int i = 0; i < frontBound; ++i){
		char* fb_iter_ptr_c = (char*)ptr+i;
		uint32_t fb_iter_ptr_i = (uint32_t)(unsigned char)*fb_iter_ptr_c;
		if(fb_iter_ptr_i !=251){
			std::stringstream ss;
			ss << "frontbound is not correct at: 0x" << std::hex << ptr;
			const std::string warningMessage = ss.str();
			printWarning(warningMessage);
			return;
		}
	}
}

void SimpleBoundsChecker::CheckBackBound(void* ptr){

	for(int i = 0; i < backBound; ++i){
		char* bb_iter_ptr_c = (char*)ptr+i;
		uint32_t bb_iter_ptr_i = (uint32_t)(unsigned char)*bb_iter_ptr_c;
		if(bb_iter_ptr_i != 187){
			std::stringstream ss;
			ss << "backbound is not correct at: 0x" << std::hex << ptr;
			const std::string warningMessage = ss.str();
			printWarning(warningMessage);
			return;
		}
	}
}


void SimpleBoundsChecker::printWarning(std::string warning){
	// logger benutzen w�re ganicht so schlecht. Hier sollte der user noch genauere Infos bekommen
	// __LINE__ , __FILE__ und dann noch welcher Stack 
	printf("WARNING: %s", warning.c_str());
}
