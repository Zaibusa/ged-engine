#include "../../../../include/moduleMemory.h"

void NoBoundsChecker::CreateFrontBound(void* ptr){}
void NoBoundsChecker::CreateBackBound(void* ptr){}
void NoBoundsChecker::CheckFrontBound(void* ptr){}
void NoBoundsChecker::CheckBackBound(void* ptr){}
void NoBoundsChecker::printWarning(std::string warning){}
NoBoundsChecker::~NoBoundsChecker(){}