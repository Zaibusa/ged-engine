#include "../../../../include/moduleMemory.h"

//Create new trackingInfo and append it to the registeredTrackings vector
void MasterMemoryTracker::OnAllocation(void* allocation, size_t size, size_t alignment, SourceInfo sourceInfo){
    trackingInfo newTrack = {allocation, size, alignment, sourceInfo};
    registeredTrackings.push_back(newTrack);
}

//Look for given allocation in tracked allocations. Remove once found
void MasterMemoryTracker::OnDeallocation(void* allocation){
    for(int i = 0; registeredTrackings.size();i++){
        if(allocation == registeredTrackings.at(i).allocation)
        {
            registeredTrackings.erase(registeredTrackings.begin()+i);
            break;
        }
    }
}
//returns vector of tracked allocation structs
std::vector<MasterMemoryTracker::trackingInfo> MasterMemoryTracker::getRegisteredTrackings(){
    return registeredTrackings;
}