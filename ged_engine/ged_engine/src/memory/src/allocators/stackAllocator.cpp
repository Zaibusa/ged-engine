#include "../../../../include/moduleMemory.h"

static const size_t PTR_OFFSET = sizeof(size_t);
static const size_t PTR_ALLOC_SIZE = sizeof(size_t);


StackAllocator::StackAllocator(size_t size, size_t alignment, size_t boundSize)
{
	// Size and alignment have to be bigger than zero. If not, return.
	if (size == 0 || alignment == 0)
		return;

	// Alignment must be power of two. If not, return. See description after the constructor.
	if ((alignment & (alignment -1) ) != 0)
		return;

	// Allocate space for (size + alignment-1). This is necessary to have at least "size" space available after aligning the stack
	size_t allocationSize = size + --alignment + PTR_OFFSET + PTR_ALLOC_SIZE + boundSize;
	m_start = (char*)malloc(allocationSize);

	// Align the start pointer and cast it into a char*. The result is our current (free) pointer. The end pointer is current + size.
    m_current = (char*)(((size_t)m_start + alignment) & ~alignment );
	m_end = m_current + allocationSize;
}

StackAllocator::~StackAllocator(){
	freeStack();
}

void* StackAllocator::Allocate(size_t size, size_t alignment, size_t front)
{
	// Calculate the current offset from the beginning of the (unaligned) stack
	uint32_t current_offset = static_cast<uint32_t>(m_current - m_start);

	uint32_t offsetPointer = (uint32_t)m_current + PTR_OFFSET + PTR_ALLOC_SIZE + front;

	// Align the pointer, see Constructor (MemoryStack) for further information
	m_current = (char*)((offsetPointer + (alignment-1) ) & ~(alignment-1) );

	void* returnPointer = m_current;

	// If there is no more memory available, return a nullpointer.
	if((m_current + size) > m_end)
		return nullptr;

	m_current -= (PTR_OFFSET + PTR_ALLOC_SIZE + front);

	// Write the offset information to the first 4 bytes of the current memory, then increase the current pointer by the offset
	uint32_t* offsetPtr = (uint32_t*)m_current;
	*offsetPtr = current_offset;
	m_current += PTR_OFFSET;

	// Write the allocation size information and increase the pointer
	uint32_t* sizePtr = (uint32_t*)m_current;
	*sizePtr = size;
	m_current += PTR_ALLOC_SIZE;

	// Save the current pointer, return it and afterwards increment the pointer for later usage
	m_current += size;
	return returnPointer;
}

void StackAllocator::Free(void* ptr)
{
	if(!ptr)
		return;

	char* offsetPtr = (char*)ptr - PTR_OFFSET - PTR_ALLOC_SIZE;
	uint32_t off = *offsetPtr;
	m_current = m_start + off;
}

void StackAllocator::freeStack()
{
	free((void*)m_start);
}

size_t StackAllocator::getAllocationSize(void* ptr)
{
	char* sizePtr = (char*)ptr - PTR_ALLOC_SIZE;
	uint32_t size = (uint32_t)*sizePtr;
	return size;
}