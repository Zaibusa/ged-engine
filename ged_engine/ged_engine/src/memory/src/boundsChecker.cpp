#include "../../../include/moduleMemory.h"

BoundsChecker::BoundsChecker(){}

BoundsChecker::~BoundsChecker(){}

BoundsChecker::BoundsChecker(size_t front, size_t back){
	this->frontBound = front;
	this->backBound = back;
}

size_t BoundsChecker::getFrontBound(){
	return frontBound;
}

size_t BoundsChecker::getBackBound(){
    return backBound;
}