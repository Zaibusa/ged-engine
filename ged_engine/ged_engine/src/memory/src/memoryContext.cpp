#include "../../../include/moduleMemory.h"

MemoryContext::MemoryContext(Allocator* a, BoundsChecker* b, MemoryTracker* m){
    allocator = a;
    boundsChecker = b;
    memoryTracker = m;
}

MemoryContext::~MemoryContext(){

}


void* MemoryContext::Allocate(size_t t, size_t alignment, SourceInfo sourceInfo){
    size_t frontBound = boundsChecker->getFrontBound();
    size_t backBound = boundsChecker->getBackBound();
    size_t size = t + frontBound + backBound;
    void * ptr = allocator->Allocate(size, alignment, frontBound);
    boundsChecker->CreateBackBound((char*)ptr + t);
    boundsChecker->CreateFrontBound((char*)ptr - frontBound);
    memoryTracker->OnAllocation(ptr, t, alignment, sourceInfo);
	return ptr;
}

void MemoryContext::Free(void* ptr){
    size_t frontBound = boundsChecker->getFrontBound();
    size_t backBound = boundsChecker->getBackBound();
    size_t totalSize = allocator->getAllocationSize(((char*)ptr - frontBound)); //user content + backBound + frontBound

	boundsChecker->CheckFrontBound(((char*)ptr - frontBound));
    boundsChecker->CheckBackBound(((char*)ptr + totalSize - frontBound - backBound));
    
    memoryTracker->OnDeallocation(ptr);
    allocator->Free((char*)ptr - frontBound);
}
