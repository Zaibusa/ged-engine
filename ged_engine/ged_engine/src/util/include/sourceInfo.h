class SourceInfo
{
public:
	SourceInfo(int line, std::string file);
	int line;
	std::string file;
};