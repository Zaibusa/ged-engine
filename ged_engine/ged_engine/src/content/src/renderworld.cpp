#include "../../../include/moduleContent.h"

//typedef unsigned int MeshID;
//static const unsigned int MAX_MESH_COUNT = 256;

//struct Mesh
//{
//	// the mesh usually has several members (vertex buffer, index buffer, etc.).
//	// for our exercise we only use a dummy member. this is only used to check whether the implementation works.
//	int dummy;
//};

// in our exercise, the RenderWorld only holds meshes, nothing more.
// the RenderWorld implementation makes sure that after *each* operation its internal data structure has no holes,
// and contains Mesh instances which are stored contiguously in memory.
	RenderWorld::RenderWorld(void)
		: m_meshCount(0),
		m_firstFree(0),
		m_lastMesh(0)
	{
		for (unsigned int i = 0; i < MAX_MESH_COUNT; i++)
		{
			m_indices[i].id = i;				// Initialize the id from 0 to 255
			m_indices[i].nextFree = i + 1;		// Next free is id + 1
			m_indices[i].meshIndex = UINT_MAX;	// meshIndex points to UINT_MAX, our implementation of no link
			m_meshLinks[i] = UINT_MAX;
		}
	}


	MeshID RenderWorld::AddMesh(void)
	{
		// TODO: add your implementation here.
		// thoroughly comment *what* you do, and *why* you do it (in german or english).

		// Add a mesh at the first free position and set the (link/index) to the first free position in the mesh array

		// If the firstFree points to a space bigger than MAX, the index array is full
		if(m_firstFree >= MAX_MESH_COUNT)
			return UINT_MAX;

		// Get the index at the first free position and link to the first free position in mesh array
		Index &ind = m_indices[m_firstFree];
		ind.meshIndex = m_lastMesh;

		// Link from mesh array to the index array
		m_meshLinks[m_lastMesh] = ind.id;

		// Increase the first free mesh position and set the first free position, stored in ind.nextFree. Increase the meshcount
		m_lastMesh++;
		m_firstFree = ind.nextFree;
		m_meshCount++;

		return ind.id;
	}


	void RenderWorld::RemoveMesh(MeshID id)
	{
		// TODO: add your implementation here.
		// thoroughly comment *what* you do, and *why* you do it (in german or english).

		// Lookup the mesh id. The index array is built in a way, that id % MAX is always stored at the same position
		// This way, lookup is possible in O(1). This could be implemented with a bit mask for better performance.

		// Get the index depending on the id modulo MAX.
		Index &ind = m_indices[id % MAX_MESH_COUNT];

		// If the id stored in the index array is not equal to the argument id, the id has changed
		// and the requested mesh is no longer available
		if(ind.meshIndex == UINT_MAX || id != ind.id)
			return;

		unsigned int meshIndex = ind.meshIndex;

		// Decrease last mesh position and mesh count
		m_lastMesh--;
		m_meshCount--;

		// Get the index in the index array from the mesh that will be moved
		// Store the new position of the mesh that will be moved in the index array
		// This is necessary to reference from the id back to the index array
		unsigned int movedIndexId = m_meshLinks[m_lastMesh];
		m_indices[movedIndexId % MAX_MESH_COUNT].meshIndex -= (m_lastMesh - meshIndex);

		// Overwrite the mesh to be deleted with the data from the last mesh
		// Moving only occurs when deleting. This way there can not be any holes in the mesh array
		// The data from the "moved" mesh is not deleted but will be overwritten in the next add.

		Mesh& meshToDelete = m_meshes[meshIndex];
		Mesh meshToMove = m_meshes[m_lastMesh];
		meshToDelete = meshToMove;

		// Increase the local id in the index array by MAX
		ind.id += MAX_MESH_COUNT;

		// The index from the deleted id now points to UINT_MAX (empty)
		ind.meshIndex = UINT_MAX;

		// Next free points to firstFree and the deleted id is the new first free
		ind.nextFree = m_firstFree;
		m_firstFree = ind.id % MAX_MESH_COUNT;
		
	}

	Mesh* RenderWorld::Lookup(MeshID id)
	{
		// TODO: add your implementation here.
		// thoroughly comment *what* you do, and *why* you do it (in german or english).

		// Get the index depending on the id modulo MAX. This way id (+= MAX) is always at the same index)
		Index ind = m_indices[id % MAX_MESH_COUNT];
		unsigned int meshIndex = ind.meshIndex;

		// If the id stored in the index array is not equal to the argument id, the id has changed
		// and the requested mesh is no longer available
		if(meshIndex == UINT_MAX || id != ind.id)
			return nullptr;

		// Return a pointer to the mesh at the position stored in the index array
		Mesh* mesh = &m_meshes[meshIndex];
		return mesh;
	}


	// DO NOT CHANGE!
	// the implementation of this method needs to stay as it is.
	// you need to correctly implement all other methods to ensure that:
	// a) m_meshCount is up-to-date
	// b) m_meshes stores instances of Mesh contiguously, without any holes
	// c) external MeshIDs still refer to the correct instances
	void RenderWorld::Iterate(void)
	{
		for (unsigned int i=0; i<m_meshCount; ++i)
		{
			printf("Mesh instance %d: dummy = %d\n", i, m_meshes[i].dummy);
		}
	}