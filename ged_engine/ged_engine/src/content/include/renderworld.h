typedef unsigned int MeshID;

static const unsigned int MAX_MESH_COUNT = 256;

struct Mesh
{
	// the mesh usually has several members (vertex buffer, index buffer, etc.).
	// for our exercise we only use a dummy member. this is only used to check whether the implementation works.
	int dummy;
};

// in our exercise, the RenderWorld only holds meshes, nothing more.
// the RenderWorld implementation makes sure that after *each* operation its internal data structure has no holes,
// and contains Mesh instances which are stored contiguously in memory.
class RenderWorld
{
public:
	RenderWorld(void);
	// TODO: add your implementation here.
	// thoroughly comment *what* you do, and *why* you do it (in german or english).
	MeshID AddMesh(void);
	void RemoveMesh(MeshID id);
	Mesh* Lookup(MeshID id);


	// DO NOT CHANGE!
	// the implementation of this method needs to stay as it is.
	// you need to correctly implement all other methods to ensure that:
	// a) m_meshCount is up-to-date
	// b) m_meshes stores instances of Mesh contiguously, without any holes
	// c) external MeshIDs still refer to the correct instances
	void Iterate(void);


private:
	// TODO: add other data structures you may need to implement.

	struct Index								// Stores information for mapping from id to mesh
	{
		unsigned int id;						// Outer id, unique
		unsigned int meshIndex;					// Points to the index of the mesh array
		unsigned int nextFree;					// Points to the next hole in the index array
	};

	unsigned int m_firstFree;					// Index of the first empty space in the index array
	unsigned int m_lastMesh;					// Index of first empty space in the mesh array

	Index m_indices[MAX_MESH_COUNT];			// Stores the indices
	unsigned int m_meshLinks[MAX_MESH_COUNT];	// Points from mesh to index array

	// DO NOT CHANGE!
	// these two members are here to stay. see comments regarding Iterate().
	Mesh m_meshes[MAX_MESH_COUNT];
	unsigned int m_meshCount;
};