#include "../../../include/moduleLogging.h"

Appender::Appender()
{
	LogManager::getInstance().registerAppender(this);
}

Appender::~Appender()
{
	LogManager::getInstance().unregisterAppender(this);
}

void Appender::setLayout(Layout* layout){
	this->layout = layout;
}

Layout* Appender::getLayout(){
	return layout;
}

void Appender::addAppenderFilter(Filter* appenderFilter){
	appenderFilterList.push_back(appenderFilter);
}

bool Appender::removeAppenderFilter(Filter* appenderFilter){
	for(unsigned int i = 0; i < appenderFilterList.size(); ++i){
		if(appenderFilterList.at(i) == appenderFilter){
			appenderFilterList.erase(appenderFilterList.begin() + i);
			return true;
		}
	}
	return false;
}

void Appender::append(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message)
{
	bool isValid = true;

	for(std::vector<Filter*>::iterator it = appenderFilterList.begin(); it != appenderFilterList.end(); ++it)
	{
		Filter* filter = *it;
		if(!(filter->isValid(level, channel, line, fileName, elapsedTime, message)))
			isValid = false;
	}

	if(isValid)
	{
		std::string formattedString = layout->formatLogEvent(level,channel,line,fileName,elapsedTime,message);
		print(formattedString);
	}
}

void Appender::setName(std::string name)
{
	this->name = name;
}
std::string Appender::getName()
{
	return name;
}