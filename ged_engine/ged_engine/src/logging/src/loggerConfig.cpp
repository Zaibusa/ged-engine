#include "../../../include/moduleLogging.h"

LoggerConfig::LoggerConfig()
{
	LogManager::getInstance().registerConfig(this);
}

LoggerConfig::~LoggerConfig()
{
	LogManager::getInstance().unregisterConfig(this);
}

void LoggerConfig::addAppender(Appender* appender){
	appenderList.push_back(appender);
}

void LoggerConfig::addFilter(Filter* filter){
	filterList.push_back(filter);
}

void LoggerConfig::distributeLogEvent(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message)
{
	bool isValid = true;

	for(std::vector<Filter*>::iterator it = filterList.begin(); it != filterList.end(); ++it)
	{
		Filter* filter = *it;
		if(!(filter->isValid(level, channel, line, fileName, elapsedTime, message)))
			isValid = false;
	}

	if(isValid)
	{
		for(std::vector<Appender*>::iterator it = appenderList.begin(); it != appenderList.end(); ++it)
		{
			(*it)->append(level,channel,line,fileName,elapsedTime,message);
		}
	}
}