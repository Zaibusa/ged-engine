#include "../../../../include/moduleLogging.h"

FileAppender::FileAppender(std::string filename, bool newFile){
	if(filename.empty()){
		return;
	}

	this->filename = filename;
	//Create the logs directory
	if(_mkdir("logs") == 0 ){
		printf( "Directory '\\logs' was successfully created\n" );
	}

	// when a new file is requested open file and discard old content
	if(newFile){
		ofs.open("logs/" + filename,std::ofstream::trunc);
		ofs.close();
	}
}

FileAppender::~FileAppender(){

}

//print to logfile
void FileAppender::print(std::string output){
	//write to the end of the file content
	ofs.open("logs/" + filename,std::ofstream::app);
	ofs << output;
	ofs.close();
}