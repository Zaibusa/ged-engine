#include "../../../../include/moduleLogging.h"

PatternLayout::PatternLayout()
{
  pattern = "%L %m";
}

PatternLayout::~PatternLayout()
{

}

bool PatternLayout::setPattern(std::string newPattern){

  std::istringstream inputStream(newPattern);
  char ch;
  //Check new Pattern char by char. If keyword % is found, check if char behind it is valid, if not return false
  while(inputStream.get(ch)){
    switch (ch)
    {
    case '%':
      oStream << '%';
      if(!inputStream.get(ch))
      {
        return false; //Failed if there is no char behind keychar %
      }
      switch(ch){ //level L, channel c, line l, fileName f, elapsedTime T, message m    
      case '%':
        oStream << '%';
        break;
      case 'L':
        oStream << 'L';
        break;
      case 'c':
        oStream << 'c';
        break;
      case 'l':
        oStream << 'l';
        break;
      case 'f':
        oStream << 'f';
        break;
      case 'T':
        oStream << 'T';
        break;
      case 'm':
        oStream << 'm';
        break;
      default:
        return false;
      }
      break;

    default:
      oStream << ch;
      break;
      }
    }
  pattern = oStream.str();
  oStream.str(std::string());
}
    
 std::string PatternLayout::formatLogEvent(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message){
  std::istringstream inputStream(pattern);
  char ch;
  //Go through new pattern char by char and insert Logging information behind Keywords
   while(inputStream.get(ch)){
    switch (ch)
    {
    case '%':
      if(!inputStream.get(ch))
      {
        return false; //Failed if there is no char behind keychar %
      }
      switch(ch){ //level L, channel c, line l, functionName f, className C, message m
      case '%':
        oStream << '%';
        break;
      case 'L':
        oStream << getLogLevelString(level);
        break;
      case 'c':
        oStream << channel;
        break;
      case 'l':
        oStream << line;
        break;
      case 'f':
        oStream << fileName;
        break;
      case 'T':
        oStream << elapsedTime;
        break;
      case 'm':
        oStream << message;
        break;
      default:
        return false; //Shouldn't happen, string already validated
      }
      break;
    default:
      oStream << ch;
      break;
      }
    }
  formattedString = oStream.str();
  oStream.str(std::string());
  return formattedString;
 }