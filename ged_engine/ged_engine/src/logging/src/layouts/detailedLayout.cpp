#include "../../../../include/moduleLogging.h"

DetailedLayout::DetailedLayout()
{

}

DetailedLayout::~DetailedLayout()
{

}

 std::string DetailedLayout::formatLogEvent(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message){
  oStream << "[" << elapsedTime << "] " << fileName << ":" << line <<": " << "[" << channel << "] " << getLogLevelString(level) << ": " << message << std::endl;
  formattedString = oStream.str();
  oStream.str(std::string());
  return formattedString;
 }