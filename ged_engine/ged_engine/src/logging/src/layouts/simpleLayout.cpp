#include "../../../../include/moduleLogging.h"

SimpleLayout::SimpleLayout()
{

}

SimpleLayout::~SimpleLayout()
{

}

 std::string SimpleLayout::formatLogEvent(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message){
  oStream << "[" << channel << "] " << getLogLevelString(level) << ": " << message << std::endl;
  formattedString = oStream.str();
  oStream.str(std::string());
  return formattedString;
 }