#include "../../../include/moduleLogging.h"

LogManager::LogManager()
{
	timer = new Timer();
	timer->start();
	rootLogger = nullptr;
}

LogManager::~LogManager()
{
	freeResources();
	timer->stop();
	delete timer;
}

// The root logger is a feature to quickly get a simple console logger
// The whole structure/hierarchy can also be used without touching the root logger
Logger* LogManager::getRootLogger()
{
	if(rootLogger == nullptr)
	{
		Filter* noFilter = new NoFilter();
		addGlobalFilter(noFilter);
		rootLogger = new Logger("root");
		LoggerConfig* loggerConfig = new LoggerConfig();
		Appender* defaultAppender = new ConsoleAppender();
		Layout* layout = new DetailedLayout();
		defaultAppender->setLayout(layout);
		loggerConfig->addAppender(defaultAppender);
		rootLogger->setLoggerConfig(loggerConfig);
	}

	return rootLogger;
}

Logger* LogManager::getLogger(std::string loggerName)
{
	if(loggerName.compare(rootLogger->getName()) == 0)
		return rootLogger;

	for(std::vector<Logger*>::iterator it = loggerList.begin(); it != loggerList.end(); ++it) {
		if(loggerName.compare((*it)->getName()) == 0)
			return *it;
	}
	return nullptr;
}

void LogManager::registerLogger(Logger* logger)
{
	loggerList.push_back(logger);
}
bool LogManager::unregisterLogger(Logger* logger)
{
	return unregisterLogger(logger->getName());
}

bool LogManager::unregisterLogger(std::string loggerName){
	
	for(unsigned int i = 0; i < loggerList.size(); i++)
	{
		if(loggerName.compare(loggerList[i]->getName()) == 0)
		{
			loggerList.erase(loggerList.begin()+i);
			return true;
		}
	}
	return false;
}

void LogManager::addGlobalFilter(Filter* filter)
{
	globalFilterList.push_back(filter);
}

bool LogManager::removeGlobalFilter(Filter* filter)
{
	return removeGlobalFilter(filter->getName());
}

// Prototype of deleting a filter by it's name. We did not follow through with
// this feature because of the project scope (strings are not unique, etc..)
bool LogManager::removeGlobalFilter(std::string filterName)
{
	for(unsigned int i = 0; i < loggerList.size(); i++)
	{
		if(filterName.compare(globalFilterList[i]->getName()) == 0)
		{
			globalFilterList.erase(globalFilterList.begin()+i);
			return true;
		}
	}
	return false;
}

Timer* LogManager::getTimer()
{
	return timer;
}

bool LogManager::logEventIsValidatedByGlobalFilters(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message)
{
	bool isValid = true;

	for(std::vector<Filter*>::iterator it = globalFilterList.begin(); it != globalFilterList.end(); ++it)
	{
		Filter* filter = *it;

		if(!(filter->isValid(level,channel,line,fileName,elapsedTime,message)))
			isValid = false;
	}

	return isValid;
}

std::vector<Logger*> LogManager::getLoggerList()
{
	return loggerList;
}

void LogManager::registerFilter(Filter* filter)
{
	filterList.push_back(filter);
}

bool LogManager::unregisterFilter(Filter* filter)
{
	for(unsigned int i = 0; i < filterList.size(); i++)
	{
		if(filter == filterList.at(i))
		{
			filterList.erase(filterList.begin()+i);
			return true;
		}
	}
	return false;
}

void LogManager::registerAppender(Appender* appender)
{
	appenderList.push_back(appender);
}

bool LogManager::unregisterAppender(Appender* appender)
{
	for(unsigned int i = 0; i < appenderList.size(); i++)
	{
		if(appender == appenderList.at(i))
		{
			appenderList.erase(appenderList.begin()+i);
			return true;
		}
	}
	return false;
}

void LogManager::registerLayout(Layout* layout)
{
	layoutList.push_back(layout);
}

bool LogManager::unregisterLayout(Layout* layout)
{
	for(unsigned int i = 0; i < layoutList.size(); i++)
	{
		if(layout == layoutList.at(i))
		{
			layoutList.erase(layoutList.begin()+i);
			return true;
		}
	}
	return false;
}

void LogManager::registerConfig(LoggerConfig* config)
{
	configList.push_back(config);
}

bool LogManager::unregisterConfig(LoggerConfig* config)
{
	for(unsigned int i = 0; i < configList.size(); i++)
	{
		if(config == configList.at(i))
		{
			configList.erase(configList.begin()+i);
			return true;
		}
	}
	return false;
}

void LogManager::countEverything()
{
	printf("Appenders: %i\n", appenderList.size());
	printf("Loggers: %i\n", loggerList.size());
	printf("Configs: %i\n", configList.size());
	printf("Layouts: %i\n", layoutList.size());
	printf("Filters: %i\n", filterList.size());
	printf("\n");
}

void LogManager::freeResources()
{
	deleteLayouts();
	deleteAppender();
	deleteFilters();
	deleteConfigs();
	deleteLoggers();
}

void LogManager::deleteAppender()
{
	while(appenderList.size() > 0)
	{
		Appender* appender = appenderList.at(0);
		delete appender;
	}
}

void LogManager::deleteFilters()
{
	while(filterList.size() > 0)
	{
		Filter* filter = filterList.at(0);
		delete filter;
	}
}

void LogManager::deleteLoggers()
{
	while(loggerList.size() > 0)
	{
		Logger* logger = loggerList.at(0);
		delete logger;
	}
}

void LogManager::deleteLayouts()
{
	while(layoutList.size() > 0)
	{
		Layout* layout = layoutList.at(0);
		delete layout;
	}
}

void LogManager::deleteConfigs()
{
	while(configList.size() > 0)
	{
		LoggerConfig* config = configList.at(0);
		delete config;
	}
}