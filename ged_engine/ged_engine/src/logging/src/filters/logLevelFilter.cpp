#include "../../../../include/moduleLogging.h"

LogLevelFilter::LogLevelFilter()
{

}

LogLevelFilter::LogLevelFilter(LogLevel logLevel, RelationalOperator relationalOperator)
{
	this->logLevel = logLevel;
	this->relationalOperator = relationalOperator;
}

LogLevelFilter::~LogLevelFilter()
{

}

bool LogLevelFilter::isValid(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message)
{
	switch(relationalOperator)
	{
	case RelationalOperator::LESSER_EQUAL:
		if(level <= logLevel)
		{
			std::cout << "LogLevelFilter: Got a lesser equal and disapproved" << std::endl;
			return false;
		}
		break;
	case RelationalOperator::EQUAL:
		if(level == logLevel)
		{
			std::cout << "LogLevelFilter: Got a equal and disapproved" << std::endl;
			return false;
		}
		break;
	case RelationalOperator::GREATER_EQUAL:
		if(level >= logLevel)
		{
			std::cout << "LogLevelFilter: Got a greater equal and disapproved" << std::endl;
			return false;
		}
		break;
	default:
		return false;
		break;
	}
	//std::cout << "LogLevelFilter: Got a request and allowed it" << std::endl;
	return true;
}