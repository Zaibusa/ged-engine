#include "../../../include/moduleLogging.h"

Filter::Filter()
{
	LogManager::getInstance().registerFilter(this);
}

Filter::~Filter()
{
	LogManager::getInstance().unregisterFilter(this);
}

void Filter::setName(std::string name)
{
	this->name = name;
}
std::string Filter::getName()
{
	return name;
}