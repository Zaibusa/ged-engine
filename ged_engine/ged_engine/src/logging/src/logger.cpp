#include "../../../include/moduleLogging.h"

Logger::Logger()
{
	LogManager::getInstance().registerLogger(this);
}

Logger::Logger(std::string loggerName)
{
	this->name = loggerName;
	this->config = nullptr;
	this->parent = nullptr;
	this->additivity = false;
	LogManager::getInstance().registerLogger(this);
}

Logger::Logger(std::string loggerName, Logger* parent)
{
	this->name = loggerName;
	this->config = nullptr;
	this->parent = parent;
	this->additivity = true;
	LogManager::getInstance().registerLogger(this);
}

Logger::~Logger(){
	LogManager::getInstance().unregisterLogger(this);
}

void Logger::log(LogLevel level, std::string channel, std::string message, SourceInfo sourceInfo)
{
	double elapsedTime = LogManager::getInstance().getTimer()->getElapsedTimeInMilliSec();

	bool isValid = LogManager::getInstance().logEventIsValidatedByGlobalFilters(level,channel,sourceInfo.line,sourceInfo.file,elapsedTime,message);
	if(isValid)
		config->distributeLogEvent(level,channel,sourceInfo.line,sourceInfo.file,elapsedTime,message);

	if(additivity)
		parent->log(level, channel, message, sourceInfo);
}

Logger* Logger::getRootLogger()
{
	return LogManager::getInstance().getRootLogger();
}

Logger* getLogger(std::string loggerName)
{
	return LogManager::getInstance().getLogger(loggerName);
}

std::string Logger::getName()
{
	return name;
}

void Logger::setName(std::string loggerName)
{
	this->name = loggerName;
}

bool Logger::isAdditive()
{
	return additivity;
}

void Logger::setAdditive(bool additivity)
{
	this->additivity = additivity;
}

void Logger::setLoggerConfig(LoggerConfig* loggerConfig)
{
	this->config = loggerConfig;
}

LoggerConfig* Logger::getLoggerConfig()
{
	return config;
}