#include "../../../include/moduleLogging.h"

Layout::Layout()
{
	LogManager::getInstance().registerLayout(this);
}

Layout::~Layout()
{
	LogManager::getInstance().unregisterLayout(this);
}

std::string Layout::getName()
{
	return name;
}
void Layout::setName(std::string name)
{
	this->name = name;
}

std::string Layout::getLogLevelString(LogLevel level)
{
	switch(level)
	{
	case 0:
		return "TRACE";
	case 1:
		return "DEBUG";
	case 2:
		return "INFO";
	case 3:
		return "WARN";
	case 4:
		return "ERROR";
	case 5:
		return "FATAL";
	default:
		return "UNDEFINED";
	}
}