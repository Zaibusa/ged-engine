class Filter
{
public:
	Filter();
	virtual ~Filter();

	// All of the classes appender, filter, layout and logger have a name so they can be queried by that name in the log manager
	// The feature of querying is not in the code yet
	void setName(std::string name);
	std::string getName();

	virtual bool isValid(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message) = 0;

private:
	std::string name;
};