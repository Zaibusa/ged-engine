//Base Layout Class contains virtual formatLogEvent function, to be overloaded by child classes.

class Layout
{
public:
	Layout();
	virtual ~Layout();
    //Format Log Event, contains all Data sent by the logger. Returns formatted string, ready for output
	virtual std::string formatLogEvent(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message) = 0;

	// All of the classes appender, filter, layout and logger have a name so they can be queried by that name in the log manager
	// The feature of querying is not in the code yet
	std::string getName();
	void setName(std::string);
	
	std::string getLogLevelString(LogLevel level);
protected:
	std::string formattedString; // Return Value for formatLogEvent
	std::ostringstream oStream; //String to be built from input parameters
	std::string name;
};