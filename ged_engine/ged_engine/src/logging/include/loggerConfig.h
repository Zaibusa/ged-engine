class LoggerConfig
{
public:
	LoggerConfig();
	virtual ~LoggerConfig();
	
	void addAppender(Appender* appender);
	void addFilter(Filter* filter);

	void distributeLogEvent(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message);

private:
	std::vector<Appender*> appenderList;
	std::vector<Filter*> filterList;
};