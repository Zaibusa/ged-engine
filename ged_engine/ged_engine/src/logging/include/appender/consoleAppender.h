class ConsoleAppender : public Appender
{
public:
	ConsoleAppender();
	~ConsoleAppender();
	void print(std::string);
};
