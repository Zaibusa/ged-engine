class FileAppender : public Appender
{
public:
	FileAppender(std::string ,bool);
	~FileAppender();
	void print(std::string);
private:
	std::string filename;
	std::ofstream ofs;
};