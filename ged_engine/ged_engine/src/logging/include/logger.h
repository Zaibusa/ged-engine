class LogManager;

class Logger
{
public:
	Logger(std::string loggerName);
	Logger(std::string loggerName, Logger* parent);
	Logger();
	virtual ~Logger();

	static Logger* getRootLogger();
	static Logger* getLogger(std::string loggerName);

	std::string getName();
	void setName(std::string loggerName);

	bool isAdditive();
	void setAdditive(bool additivity);

	void setLoggerConfig(LoggerConfig* loggerConfig);
	LoggerConfig* getLoggerConfig();

	void log(LogLevel level, std::string channel, std::string message); // never used because of macro
	void log(LogLevel level, std::string channel, std::string, SourceInfo sourceInfo);

private:
	std::string name;
	bool additivity;

	LoggerConfig* config;
	Logger* parent;
};