//Details Layout with all Information available
class DetailedLayout : public Layout
{
public:
	DetailedLayout();
	~DetailedLayout();

	std::string formatLogEvent(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message);
};