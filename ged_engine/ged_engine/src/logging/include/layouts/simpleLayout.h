//Simple Layout, returns only Log Level, Channel and Message
class SimpleLayout : public Layout
{
public:
	SimpleLayout();
	~SimpleLayout();

	virtual std::string formatLogEvent(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message);
};