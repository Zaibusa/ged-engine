//Custom Layout that can be defined with the setPattern function
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	//Syntax for writing custom layout pattern:                                                       //
	//level L, channel c, line l, fileName f, elapsedTime T, message m                              //
	// ("Hello, this is the loglevel %L and this is the Message %m and this is a percent symbol: %%") //
    ////////////////////////////////////////////////////////////////////////////////////////////////////

class PatternLayout : public Layout
{
public:
	PatternLayout();
	~PatternLayout();

    //Formats the LogEvent according to the pattern string
    virtual std::string formatLogEvent(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message);

    //Validates a new Pattern and sets the pattern string accordingly
    bool setPattern(std::string newPattern);

private:
  std::string pattern;  
};