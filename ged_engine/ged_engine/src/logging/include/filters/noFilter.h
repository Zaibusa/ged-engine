class NoFilter : public Filter
{
public:
	bool isValid(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message);
};