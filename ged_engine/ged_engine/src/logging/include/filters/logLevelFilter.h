class LogLevelFilter : public Filter
{
public:
	LogLevelFilter();
	LogLevelFilter(LogLevel logLevel, RelationalOperator op);
	~LogLevelFilter();

	bool isValid(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message);
private:
	LogLevel logLevel;
	RelationalOperator relationalOperator;
};