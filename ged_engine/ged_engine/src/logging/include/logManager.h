class Logger;

// Every component of a logger and the logger itself registers to the log manager singleton in its
// constructor, and unregisters in the destructor. The log manager keeps track of every living component
// and destroyes every single component when the freeResources() method is called or he is destructed.
class LogManager
{

public:
    static LogManager& getInstance()
    {
        static LogManager instance;
        return instance;
    }
	~LogManager();
	
	Logger* getRootLogger();
	Logger* getLogger(std::string loggerName);

	void registerLogger(Logger* logger);
	bool unregisterLogger(Logger* logger);
	bool unregisterLogger(std::string loggerName);

	void registerFilter(Filter* filter);
	bool unregisterFilter(Filter* filter);

	void registerAppender(Appender* appender);
	bool unregisterAppender(Appender* appender);

	void registerLayout(Layout* layout);
	bool unregisterLayout(Layout* layout);

	void registerConfig(LoggerConfig* config);
	bool unregisterConfig(LoggerConfig* config);

	void addGlobalFilter(Filter* filter);
	bool removeGlobalFilter(Filter* filter);
	bool removeGlobalFilter(std::string filterName);

	std::vector<Logger*> getLoggerList();

	bool logEventIsValidatedByGlobalFilters(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message);

	Timer* getTimer();

	void countEverything();

	void freeResources();

private:
    LogManager();
    LogManager(LogManager const& copy);            // Not Implemented
    LogManager& operator=(LogManager const& copy); // Not Implemented

	void deleteAppender();
	void deleteFilters();
	void deleteLoggers();
	void deleteLayouts();
	void deleteConfigs();

	Logger* rootLogger;
	Timer* timer;
	std::vector<Filter*> globalFilterList;

	std::vector<Logger*> loggerList;
	std::vector<Filter*> filterList;
	std::vector<Appender*> appenderList;
	std::vector<Layout*> layoutList;
	std::vector<LoggerConfig*> configList;

};