class Appender
{
public:
	Appender();
	virtual ~Appender();

	void setLayout(Layout* layout);
	Layout* getLayout();

	void addAppenderFilter(Filter*);
	bool removeAppenderFilter(Filter*);

	void append(LogLevel level, std::string channel, int line, std::string fileName, double elapsedTime, std::string message);
	virtual void print(std::string) = 0;

	// All of the classes appender, filter, layout and logger have a name so they can be queried by that name in the log manager
	// The feature of querying is not in the code yet
	void setName(std::string);
	std::string getName();

private:
	Layout* layout;
	std::vector<Filter*> appenderFilterList;
	std::string name;
};