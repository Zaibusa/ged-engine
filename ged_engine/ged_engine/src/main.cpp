#include "../include/modules.h"

#define OPERATOR_NEW(type, context) new (context->Allocate(sizeof(type), 16, SourceInfo(__LINE__, __FILE__))) type
#define OPERATOR_DELETE(object, context)    OperatorHelper::Delete(object, context)

// Enable/Disable Logging (Release/Debug)
#define LOGGING_ENABLED

#ifdef LOGGING_ENABLED
    #define log(LOGGER, A, B, C) LOGGER->log(A, B, C, SourceInfo(__LINE__, __FILE__))
#else
    #define log(LOGGER,A,B,C) ((void)0)
#endif

// Enable/Disable execution of the code for the exercises
#define LOGGING 1
#define MEMORY 1
#define PACKEDARRAY 1

int main()
{
#if LOGGING
	bool result;

	// Get the default logger from the LogManager. This is a preimplemented logger for siple and fast logging
	Logger* rootLogger = Logger::getRootLogger();

	// Create a second logger, that sets the rootLogger as its parent and forwards all events to the rooLogger after processing
	Logger* secondLogger = new Logger("logger2", rootLogger);

	// Create a global filter that blocks all messages that are set on ERROR level
	Filter* levelFilter = new LogLevelFilter(LogLevel::ERR, RelationalOperator::EQUAL);
	LogManager::getInstance().addGlobalFilter(levelFilter);

	// Create a new logger config for the second Logger
	LoggerConfig* loggerConfig = new LoggerConfig();

	// Create two file Appenders. The first rewrites the file, the second appends
	Appender* fileAppender = new FileAppender("test.log", FileAppenderState::OVERWRITE_FILE);
	Appender* fileAppender2 = new FileAppender("test2.log", FileAppenderState::APPEND_FILE);

	// Create a Pattern Layout and and add it to both file appenders. Add the file Appenders to the 2 loggers
	PatternLayout* layout = new PatternLayout();
	result = layout->setPattern("Hello, this is the loglevel: %L\n and this is the Message: %m\n and this is a percent symbol: %%\n this is the channel: %c\n this is the filename: %f \n this is the line: %l \n this is the timestamp: %T \n");
	//result = layout->setPattern("test%bla%lol%"); //Invalid Layout
	if (!result)
		printf("Error: Pattern couldn't be saved\n");
	fileAppender->setLayout(layout);
	fileAppender2->setLayout(layout);
	
	// Create a new filter for Info level or lesser and add apply it to the rootLoggers file appender
	Filter* levelFilter2 = new LogLevelFilter(LogLevel::INFO, RelationalOperator::LESSER_EQUAL);
	fileAppender->addAppenderFilter(levelFilter2);

	// Add the file appenders to the loggers
	rootLogger->getLoggerConfig()->addAppender(fileAppender);
	loggerConfig->addAppender(fileAppender2);

	// Apply the new logger config to the second Logger
	secondLogger->setLoggerConfig(loggerConfig);

	// Log a Warn Message on the rootLogger
	// This message will be printed by the rootLoggers Console Appender and on the rootLoggers File Appender
	log(rootLogger, LogLevel::WARN, "First Channel", "This is just a testmessage on warn level");

	// Log a Error Message on the second logger
	// This message will be blocked by the global "levelFilter" and not be printed at all
	log(secondLogger, LogLevel::ERR, "Second Channel", "This is an error message");

	// Log a Fatal Message on the second Logger
	// This message will be printed by the second loggers file appender (test2.log) and then be forwarded to the root logger (parent)
	// The root logger will print this message by both his file appender and his console appender
	log(secondLogger, LogLevel::FATAL, "Third Channel", "This is a fatal error message");

	// Log a Info Message on the second logger
	// This message will be printed by the second loggers file appender and then be forwarded to the parent
	// The root Logger will print this message by the console appender. The levelFilter2 will block the message for the file appender
	log(secondLogger, LogLevel::INFO, "Fourth Channel", "This is a info message");

#endif

#if MEMORY

	BoundsChecker* checker = new SimpleBoundsChecker(4, 4);
	Allocator* allocator = new StackAllocator(128, 16, 8);
	MasterMemoryTracker* tracker = new MasterMemoryTracker();

	MemoryContext* context = new MemoryContext(allocator, checker, tracker);

	/*void* ptr = context->Allocate(36, 16, SourceInfo(__LINE__, __FILE__));

	context->Free(ptr);*/

	MasterMemoryTracker* testTracker = OPERATOR_NEW(MasterMemoryTracker, context)();

	std::vector<MasterMemoryTracker::trackingInfo> trackings = tracker->getRegisteredTrackings();

	for(int i = 0; i < trackings.size(); i++)
	{
		MasterMemoryTracker::trackingInfo tracking = trackings[i];
		int line = tracking.sourceInfo.line;
		std::string file = tracking.sourceInfo.file;
		std::cout << "There is still an active allocation from " << file << ":" << line << std::endl;
	}

	OPERATOR_DELETE(testTracker, context);
	delete checker;
	delete allocator;
	delete tracker;
	delete context;
#endif
#if PACKEDARRAY
	
	RenderWorld rw;

	// - array kann NICHT ganz angef�llt und wieder entleert werden
	// -> bug: danach ist bei .Iterate() der mesh-count UINT_MAX gro� (sollte 0 sein)
	// Works with this test case

	MeshID meshIds[256];

	for (int i = 0; i < 256; i++)
	{
		MeshID newId = rw.AddMesh();
		
		// An ID with UINT_MAX is our term of expressing an error, so we don't have to put an exception/assert in the code
		if(newId != UINT_MAX)
		{
			meshIds[i] = newId;
			Mesh* mesh = rw.Lookup(meshIds[i]);
			mesh->dummy = i;
		}		
	}

	rw.Iterate();

	for (int i = 0; i < 256; i++)
	{
		rw.RemoveMesh(meshIds[i]);
	}

	rw.Iterate();

	// add 3 meshes to the world. we only ever refer to them by their ID, the RenderWorld has complete ownership
	// over the individual Mesh instances.
	MeshID meshID0 = rw.AddMesh();
	MeshID meshID1 = rw.AddMesh();
	MeshID meshID2 = rw.AddMesh();

	// lookup the meshes, and fill them with data.
	{
		Mesh* mesh0 = rw.Lookup(meshID0);
		mesh0->dummy = 0;
		Mesh* mesh1 = rw.Lookup(meshID1);
		mesh1->dummy = 1;
		Mesh* mesh2 = rw.Lookup(meshID2);
		mesh2->dummy = 2;
	}

	// by now, the world contains 3 meshes, filled with dummy data 0, 1 and 2.
	// in memory, the 3 meshes should be contiguous in memory:
	// [Mesh][Mesh][Mesh]
	rw.Iterate();

	// we now remove the second mesh (referenced by meshID1), which creates a hole in the world's data structure:
	// [Mesh][Empty][Mesh]
	// the world should internally update its data structure(s), so that the other two remaining meshes are stored contiguously in memory.
	rw.RemoveMesh(meshID1);

	// iteration must still work, because the instances are contiguous in memory.
	rw.Iterate();

	// even though the render world might have copied/changed some data structures, the two remaining meshes must still
	// refer to the correct object. this is verified by checking their dummy members.
	assert(rw.Lookup(meshID0)->dummy == 0);
	assert(rw.Lookup(meshID2)->dummy == 2);

	// the mesh referenced by meshID1 has been removed above, yet we intentionally try to access it.
	// the implementation should give an error, and return a nullptr in that case.
	Mesh* mesh1 = rw.Lookup(meshID1);
	assert(mesh1 == nullptr);
	
	MeshID meshID257 = rw.AddMesh();
	Mesh* mesh257 = rw.Lookup(meshID257);
	mesh257->dummy = 257;

	MeshID meshID3 = rw.AddMesh();
	Mesh* mesh3 = rw.Lookup(meshID3);
	mesh3->dummy = 3;

	rw.RemoveMesh(meshID0);
		
	 //Index array: 
	 //id:   |256|257| 2 | 3 | 4 | ...
	 //next: | 4 |   |   |   | 5 | ...
	 //index:|   | 2 | 1 | 0 |   | ...

	 //Mesh array:
	 //id:   | 3 | 2 |257|   |   | ...

#endif
	
	std::cin.get();
	printf("Program finished");
	return 0;



}