// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

enum LogLevel {TRACE, DEBUG, INFO, WARN, ERR, FATAL};
enum RelationalOperator {LESSER_EQUAL, EQUAL, GREATER_EQUAL};
enum FileAppenderState {APPEND_FILE, OVERWRITE_FILE};

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>

#include <direct.h>

#include "../src/util/include/moduleUtil.h"

#include "../src/logging/include/layout.h"
#include "../src/logging/include/filter.h"

#include "../src/logging/include/appender.h"
#include "../src/logging/include/appender/consoleAppender.h"
#include "../src/logging/include/appender/fileAppender.h"

#include "../src/logging/include/loggerConfig.h"
#include "../src/logging/include/logger.h"
#include "../src/logging/include/LogManager.h"

#include "../src/logging/include/filters/noFilter.h"
#include "../src/logging/include/filters/logLevelFilter.h"

#include "../src/logging/include/layouts/detailedLayout.h"
#include "../src/logging/include/layouts/simpleLayout.h"
#include "../src/logging/include/layouts/patternLayout.h"

