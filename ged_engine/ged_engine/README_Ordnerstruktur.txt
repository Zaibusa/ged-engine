Architektur des Projekts:
/include/ 			Enth�lt die Top-Level Includes, die notwendig sind um entweder die ganze Engine "engine.h" oder
				einzelne Module in einem Projekt zu includen (zB "logging.h")
/src/				Enth�lt s�mtlichen Sourcecode und ist in Module unterteilt
/src/*Modul*/			Ein einzelnes Modul, zB Logging, Memory, ...
/src/*Modul*/src		Sourcecode des Moduls
/src/*Modul*/include		Header des Moduls
/src/*Modul*/src/Layouts	Sourcecode der einzelnen Layouts
/src/*Modul*/include/Layouts	Header der einzelnen Layouts